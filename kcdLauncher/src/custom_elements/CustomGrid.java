package custom_elements;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import com.kioskmaker.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomGrid extends BaseAdapter{
    private Context mContext;
    private final String[] isimler;
    private final long[] itemId;
    private final String[] ifAssetsImages;
private final Activity activity;
      public CustomGrid(Context c,Activity activity, String[] isimler,long[] itemId,String[] ifAssetsImage_path ) {
          mContext = c;
          this.activity=activity;
          this.itemId = itemId;
          this.isimler = isimler;
          this.ifAssetsImages=ifAssetsImage_path;
      }

      @Override
      public int getCount() {
          // TODO Auto-generated method stub
          return isimler.length;
      }

      @Override
      public Object getItem(int position) {
          // TODO Auto-generated method stub
          return null;
      }

      @Override
      public long getItemId(int position) {
          // TODO Auto-generated method stub
          return 0;
      }

      @Override
      public View getView(int position, View convertView, ViewGroup parent) {
          // TODO Auto-generated method stub
          View grid;
          LayoutInflater inflater = (LayoutInflater) mContext
              .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

          //burayı iptal ettim pozisyonkayması oluyordu
//          if (convertView == null) {

              grid = new View(mContext);
              grid = inflater.inflate(R.layout.grid_item, null);
              TextView textView = (TextView) grid.findViewById(R.id.grid_text);
              ImageView imageView = (ImageView)grid.findViewById(R.id.grid_image);
              textView.setText(isimler[position]);
              //imageView.setImageResource(Imageid[position]);
              //imageView.setImageDrawable(loadDrawableFromAssets(mContext, ifAssetsImages[position]));
              //imageView.setImageBitmap(loadBitmapFromAssets(mContext, ifAssetsImages[position]));
              
              
              
              
              
              
              
              
              //File filePath =mContext.getFileStreamPath(ifAssetsImages[position]);
              //imageView.setImageDrawable(Drawable.createFromPath(filePath.toString()));
              if(!ifAssetsImages[position].contains("resimyok"))
              try {
            	  
            	  FileInputStream fis;
      			
  				fis = mContext.openFileInput(ifAssetsImages[position]
  							);
  			 
                Bitmap bitmapx;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                bitmapx = BitmapFactory.decodeStream(fis, null, options);
                
                int imageHeight = options.outHeight;
                int imageWidth = options.outWidth;
                Log.e("yukl", String.valueOf(imageHeight));
                Log.e("width", String.valueOf(imageWidth));
                
                
                // recreate the stream
                // make some calculation to define inSampleSize
                
                
                DisplayMetrics metrics = new DisplayMetrics();
                
                ((Activity) activity).getWindowManager().getDefaultDisplay().getMetrics(metrics);
                int screenWidth = metrics.widthPixels;
                int screenHeight =metrics.heightPixels;
//                
               
                
                
                
               options.inSampleSize = calculateInSampleSize(options,screenWidth/4,screenHeight/3);
                //options.inSampleSize = calculateInSampleSize(options,imageHeight/4,imageWidth/4);
                options.inJustDecodeBounds = false;
                options.inPreferredConfig = Config.RGB_565;
                
                Bitmap bitmap = BitmapFactory.decodeStream(mContext.openFileInput(ifAssetsImages[position]
							), null, options);
                
            	  
            	  
				imageView.setImageBitmap(bitmap);
				
				 imageHeight = options.outHeight;
	                imageWidth = options.outWidth;
	                Log.e("yuklyy", String.valueOf(imageHeight));
	                Log.e("widthyy", String.valueOf(imageWidth));
	                
	                
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				Log.e("hatabune", e.getMessage());
				//e.printStackTrace();
			}
              else
              {
            	  
            	  imageView.setImageBitmap(loadBitmapFromAssets(mContext, "kcd_resimler/gruplar/g1.jpg"));
              }
              
//          } else {
//              grid = (View) convertView;
//          }

          return grid;
      }
      
      public static int calculateInSampleSize(
              BitmapFactory.Options options, int reqWidth, int reqHeight) {
      // Raw height and width of image
      final int height = options.outHeight;
      final int width = options.outWidth;
      int inSampleSize = 1;
   
      if (height > reqHeight || width > reqWidth) {
   
          // Calculate ratios of height and width to requested height and width
          final int heightRatio = Math.round((float) height / (float) reqHeight);
          final int widthRatio = Math.round((float) width / (float) reqWidth);
   
          // Choose the smallest ratio as inSampleSize value, this will guarantee
          // a final image with both dimensions larger than or equal to the
          // requested height and width.
          inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
      }
   
      return inSampleSize;
  }
      
      
      
      private Drawable loadDrawableFromAssets(Context context, String path)
      {
          InputStream stream = null;
          try
          {
              stream = context.getAssets().open(path);
              return Drawable.createFromStream(stream, null);
          }
          catch (Exception ignored) {
        	  
        	  Log.e("hataaa", ignored.getMessage());
        	  
          } finally
          {
        	  
              try
              {
                  if(stream != null)
                  {
                      stream.close();
                  }
              } catch (Exception ignored) {}
          }
          return null;
      }
      private Bitmap loadBitmapFromAssets(Context context, String path)
      {
          InputStream stream = null;
          try
          {
              stream = context.getAssets().open(path);
              
              
              
              Bitmap bitmapx;
              BitmapFactory.Options options = new BitmapFactory.Options();
              options.inJustDecodeBounds = true;
              bitmapx = BitmapFactory.decodeStream(stream, null, options);
              
              int imageHeight = options.outHeight;
              int imageWidth = options.outWidth;
              Log.e("yukl", String.valueOf(imageHeight));
              Log.e("width", String.valueOf(imageWidth));
              
              
              // recreate the stream
              // make some calculation to define inSampleSize
              
              
              DisplayMetrics metrics = new DisplayMetrics();
              
              ((Activity) activity).getWindowManager().getDefaultDisplay().getMetrics(metrics);
              int screenWidth = metrics.widthPixels;
              int screenHeight =metrics.heightPixels;
              
             
              
              
              options.inSampleSize = calculateInSampleSize(options,screenWidth/4,screenHeight/3);
              options.inJustDecodeBounds = false;
              options.inPreferredConfig = Config.RGB_565;
              
             
              
              
              
              
              
              return  BitmapFactory.decodeStream(stream, null, options);
              
              
          }
          catch (Exception ignored) {} finally
          {
              try
              {
                  if(stream != null)
                  {
                      stream.close();
                  }
              } catch (Exception ignored) {}
          }
          return null;
      }
}