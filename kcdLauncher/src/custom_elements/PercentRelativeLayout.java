package custom_elements;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public class PercentRelativeLayout extends RelativeLayout {
    private final PercentLayoutHelper mHelper;

    public PercentRelativeLayout(Context context) {
    	
        super(context);
        if(isInEditMode()){
            Log.e("dsfsd", "sdfsdf");
         }
        Log.e("çalışıyormu", "oldumu");
    	System.out.println("deneme");
    	this.requestLayout();
    	
        mHelper = new PercentLayoutHelper(this);
    }

    
    
    public PercentRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        
        
        if(isInEditMode()){
            
         }
        
        Log.e("çalışıyormu", "oldumu");
    	System.out.println("deneme");
    	
    	
    	this.requestLayout();
    	
    	
        mHelper = new PercentLayoutHelper(this);
    }
    
//
//    @Override
//    protected void onDraw(Canvas canvas) {
//    	// TODO Auto-generated method stub
//    	
//    	super.onDraw(canvas);
//    	if(isInEditMode()){
//            Log.e("dsfsd", "sdfsdf");
//         }
//    	Paint paint = new Paint();
//        final RectF rect = new RectF();
//        //Example values
//        rect.set(0,	0 , 300 ,300); 
//        paint.setColor(Color.GREEN);
//        paint.setStrokeWidth(20);
//        paint.setAntiAlias(true);
//        paint.setStrokeCap(Paint.Cap.ROUND);
//        paint.setStyle(Paint.Style.STROKE);
//        canvas.drawArc(rect, 0, 360, false, paint);
//        
//    	
//    }
    
    private Paint mPaint;
   
   
   
    
    
//    @Override
//    protected void dispatchDraw(Canvas canvas) {
//    	
//    	
//        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
//        mPaint.setStrokeWidth(5);
//        mPaint.setColor(0xffffffff);
//    	
//        int cnt = getChildCount();
//        for (int i = 0; i < cnt; i++) {
//            View child = getChildAt(i);
//            int l = child.getLeft();
//            int t = child.getTop();
//            int r = child.getRight();
//            int b = child.getBottom();
//            if (i % 2 == 0) {
//                canvas.drawLine(l, t, r, b, mPaint);
//            } else {
//                canvas.drawLine(l, b, r, t, mPaint);
//            }
//        }
//        super.dispatchDraw(canvas);
//    }
    
    
    public LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new LayoutParams(getContext(), attrs);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    	Log.e("çalışıyormu", "oldumu");
    	System.out.println("deneme");
    	
        this.mHelper.adjustChildren(widthMeasureSpec, heightMeasureSpec);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (this.mHelper.handleMeasuredStateTooSmall())
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    protected void onLayout(boolean changed, int left, int top, int right,
            int bottom) {
    	Log.e("çalışıyormu", "oldumu");
    	System.out.println("deneme");
    	
        super.onLayout(changed, left, top, right, bottom);
        this.mHelper.restoreOriginalParams();
    }

    public static class LayoutParams extends RelativeLayout.LayoutParams
            implements PercentLayoutHelper.PercentLayoutParams {
        private PercentLayoutHelper.PercentLayoutInfo mPercentLayoutInfo;

        public LayoutParams(Context c, AttributeSet attrs) {
            super(c, attrs);
            this.mPercentLayoutInfo = PercentLayoutHelper.getPercentLayoutInfo(
                    c, attrs);
        }

        public LayoutParams(int width, int height) {
            super(width, height);
        }

        public LayoutParams(ViewGroup.LayoutParams source) {
            super(source);
        }

        public LayoutParams(MarginLayoutParams source) {
            super(source);
        }

        public PercentLayoutHelper.PercentLayoutInfo getPercentLayoutInfo() {
            return this.mPercentLayoutInfo;
        }

        protected void setBaseAttributes(TypedArray a, int widthAttr,
                int heightAttr) {
            PercentLayoutHelper.fetchWidthAndHeight(this, a, widthAttr,
                    heightAttr);
        }
    }
}
