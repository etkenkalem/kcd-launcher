package com.kioskmaker;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import kcdnetlibrary.GenelSabitler;
import kcdnetlibrary.HttpGetYardimci;
import kcdnetlibrary.MacAdressGetirici;
import kcdnetlibrary.NetParametresi;

public class LoginActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
	}
	
	
	public Handler loginAracisi= new Handler(){
		
		public void handleMessage(android.os.Message msg) {
			
		switch(msg.what)
		{
		
		case 1:{
			
			
			Bundle gelenMesaj= msg.getData();
			
			String islemSonucu= gelenMesaj.getString("sonuc");
			
			//yav arkada� ba�taki '"' i�areti ve sondaki t�rnak i�areti JsonObject olmas�n� engelliyor.
			islemSonucu= islemSonucu.substring(1,islemSonucu.length()-1).replace("\\", "");
			
			
			Log.e("ikinciSonuc", islemSonucu);
			//islem sonucu datas�ndan gelen json stringi okuyup yorumlayacak
			
			try {
				 
				JSONObject jObj=new JSONObject(islemSonucu);
				Log.e("j1","j1");
				String durum= jObj.getString("durum");
				Log.e("j2","j2");
				if(durum.contains("basarisiz"))
				{
					TextView txtDurum= (TextView) findViewById(R.id.txtDurum);
					txtDurum.setText("Beklenmedik Bir Hatayla Kar��la��ld�...A��klama:"+jObj.getString("durumAciklama"));
				}
				else
				{
					Log.e("j3","j3");
					Intent i = new Intent();
			       // i.putExtra("key", CPU);
			        setResult(1, i);
					
					TextView txtDurum= (TextView) findViewById(R.id.txtDurum);
					txtDurum.setText("Cihaz Kay�t Edildi...");
					
					String sifre=gelenMesaj.getString("sifre");
					String mac=gelenMesaj.getString("mac");
					
					SharedPreferences sharedPref = getSharedPreferences("kcdData",Context.MODE_PRIVATE);
					SharedPreferences.Editor editor = sharedPref.edit();
					editor.putString("sifre", sifre);
					editor.putString("mac", mac);
					editor.putBoolean("kayitlimi", true);
					editor.commit();
					
					
					finish();
					break;
					
				}

				
				
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				TextView txtDurum= (TextView) findViewById(R.id.txtDurum);
				txtDurum.setText("Beklenmedik Bir Hatayla Kar��la��ld�...Hata Kodu:jSN01");
				break;
			}
			
			break;
		}
		case -1:{
			TextView txtDurum= (TextView) findViewById(R.id.txtDurum);
			txtDurum.setText("Beklenmedik Bir Hatayla Kar��la��ld�...");
			break;
			
		}
		default:{
			TextView txtDurum= (TextView) findViewById(R.id.txtDurum);
			txtDurum.setText("Beklenmedik Bir Hatayla Kar��la��ld�...");
			break;
		}
		
		
		}
			
			
			
		};
		
		
	};
	
	
	
	public class islemleriYap extends Thread
	{

		private String mac="";
		private String sifre="";
		Handler araciGelen;
		Context ctx;
		
		public islemleriYap(String sifre, Handler araciGelen,Context ctx) {
			// 
			
			this.araciGelen=araciGelen;
			this.sifre=sifre;
			this.ctx=ctx;
		}
		
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			super.run();
			
			
			HttpGetYardimci httpGetYardimci= new HttpGetYardimci();
			
			String GET_URL=GenelSabitler.hostAdresi+ "/api/kioskAndroid/cihazKayit";
			
			
			NetParametresi[] parametreler= new NetParametresi[2];
			
			NetParametresi mac= new NetParametresi();
			mac.parametreIsmi="mac";
			try
			{
			mac.parameterDegeri=new MacAdressGetirici().macGetir(getApplicationContext());
			System.out.println(mac.parameterDegeri);
			}
			catch(Exception ex)
			{
				mac.parameterDegeri="bos";
				
				
				
				//return;
				
			}
			
			NetParametresi sifre= new NetParametresi();
			sifre.parametreIsmi="sifre";
			sifre.parameterDegeri=this.sifre;
			
			
			parametreler[0]= mac;
			parametreler[1]=sifre;
			
			
				try {
					String sonuc= httpGetYardimci.getSorgusuYap(GET_URL, parametreler);
					
					Message mesaj= new Message();
					Bundle tutucu= new Bundle();
					tutucu.putString("sonuc", sonuc);
					tutucu.putString("sifre", sifre.parameterDegeri);
					tutucu.putString("mac", mac.parameterDegeri);
					mesaj.what=1;
					
					mesaj.setData(tutucu);
					
					
					araciGelen.sendMessage(mesaj);
				} catch (IOException e) {
					
						araciGelen.sendEmptyMessage(-1);
					
					Log.e("hatavarla", "hata");
					
					
				}
			
				
			
		}
		
		
		
	}
	
	
	public void girisYap(View v)
	{
		
		
		
		EditText txtSifre = (EditText) findViewById(R.id.edtSifre);
		TextView txtDurum= (TextView) findViewById(R.id.txtDurum);
		
		new islemleriYap(txtSifre.getText().toString(),loginAracisi,getApplicationContext()).start();
		
		Toast.makeText(getApplicationContext(), "L�tfen Bekleyiniz...", Toast.LENGTH_LONG).show();
		
		
	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
	//	super.onBackPressed();
	}

}
