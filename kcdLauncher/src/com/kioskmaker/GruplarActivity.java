package com.kioskmaker;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;
import custom_elements.CustomGrid;
import kcdnetlibrary.DosyaIndirici;
import kcdnetlibrary.GenelSabitler;
import kcdnetlibrary.HttpGetYardimci;
import kcdnetlibrary.NetParametresi;

public class GruplarActivity extends Activity {

	
	
	GridView grid;
    
	String[] isim = {
            "2013 COLLECTIONS",
            "KONSOL GRUBU",
            "�AY BAH�ES� GRUBU",
            "OBJE GRUBU",
            "PORSELEN SOFRA GRUBU",
            "SOFRA BARDAK & SET GRUBU" 
    };
    
    long[] itemId = {
            R.drawable.ayarlar,
            R.drawable.kampanyalar,
            R.drawable.hakkimizda,
            R.drawable.kcdlogo,
            R.drawable.sikayet,
            R.drawable.urun2
    };
    
    String[] assestsImages={
    		"kcd_resimler/gruplar/g1.jpg",
    		"kcd_resimler/gruplar/g2.jpg",
    		"kcd_resimler/gruplar/g3.jpg",
    		"kcd_resimler/gruplar/g4.jpg",
    		"kcd_resimler/gruplar/g5.jpg",
    		"kcd_resimler/gruplar/g6.jpg",    		
    };
 
	
    public void gridiDoldur()
    {
    	CustomGrid adapter = new CustomGrid(GruplarActivity.this,
    			GruplarActivity.this,
    			isim, itemId,assestsImages);
        grid=(GridView)findViewById(R.id.grid_gruplar_desenler);
                grid.setAdapter(adapter);
                grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
 
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        Toast.makeText(GruplarActivity.this, "T�klanan " +isim[position], Toast.LENGTH_SHORT).show();
                        
                        Intent niyet = new Intent(getBaseContext(),DesenlerActivity.class);
                        niyet.putExtra("itemId", itemId[position]);
                        startActivity(niyet);
                        
 
                    }
                });
    	
    }
    
    public Handler threadNesnesi= new Handler(){
    	
    	public void handleMessage(android.os.Message msg) {
    	
    		switch (msg.what) {
			case 1:
			{
				gridiDoldur();
				
				break;
			}
			case -1:
			{
				
				break;
			}

			default:
				break;
			}
    		
    		
    	};
    	
    };
   
    
    
    public class gruplarJsonAl extends Thread
    {
    	Handler threadNesnesiAlinan;
    	Context ctx;
    	public gruplarJsonAl(Handler threadNesnesiGelen,Context ctx) {
		
    		this.threadNesnesiAlinan=threadNesnesiGelen;
    		this.ctx=ctx;
		}
    	
    	@Override
    	public void run() {
    	
    		super.run();
    		
    		HttpGetYardimci hy= new HttpGetYardimci();
    		String GET_URL=GenelSabitler.hostAdresi+"/api/kioskAndroid/gruplar";
    		NetParametresi[] parametreler= new NetParametresi[2];    		
    		
    		NetParametresi npSifre= new NetParametresi();
    		npSifre.parametreIsmi="sifre";
    		NetParametresi npMac= new NetParametresi();
    		npMac.parametreIsmi="mac";
    		
    		
    		
    		
    		SharedPreferences sp= getSharedPreferences("kcdData", Context.MODE_PRIVATE);
    		
    		
    		
    		npSifre.parameterDegeri= sp.getString("sifre", "");
    		npMac.parameterDegeri= sp.getString("mac", "");
    		
    		
    		Log.e("sifre", npSifre.parameterDegeri);
    		Log.e("sifre", npMac.parameterDegeri);
    		
    		parametreler[0]=npMac;
    		parametreler[1]=npSifre;
    		
    		
    		
    		try {
				
    			String sonuc= hy.getSorgusuYap(GET_URL, parametreler);
    			
    			sonuc= sonuc.substring(1,sonuc.length()-1).replace("\\","");
    			
    			Log.e("data", sonuc);
    			
    			JSONObject jObj= new JSONObject(sonuc);
    			
    			String durum= jObj.getString("durum");
    			if(!durum.contains("basarisiz"))
    			{
    				
    				JSONArray dizi=jObj.getJSONArray("veri");
    				
    				Log.e("dizi", dizi.toString());
    				
    				int diziBoyutu= dizi.length();
    				
    				isim= new String[diziBoyutu];
    				itemId= new long[diziBoyutu];
    				assestsImages= new String[diziBoyutu];
    				
    				for(int i=0; i<diziBoyutu;i++)
    				{
    					JSONObject item= dizi.getJSONObject(i);
    					Log.e("Jarrayobje", item.toString());
    					
    					isim[i]=item.getString("KategoriAdi");
    					itemId[i]=item.getLong("KategoriID");
    					
    					String UrunSinifKodu= item.getString("UrunSinifKodu");
    					
    					try
    					{
    					DosyaIndirici.downloadFileForAndroid("http://www.kcd.com.tr/urunfoto/"+UrunSinifKodu+"-xl7.jpg", "gruplar", getApplicationContext());
    					assestsImages[i]="gruplar_"+UrunSinifKodu+"-xl7.jpg";
    					}
    					catch(Exception ex)
    					{
    						Log.e("resim indirilirken hata", "hata@@@@@@@@"+ex.getMessage());
    						assestsImages[i]="resimyok";
    					}
    					
    					
    					//TODO de�i�tirilecek
    					//assestsImages[i]="kcd_resimler/gruplar/g1.jpg";
    					//assestsImages[i]="gruplar/"+UrunSinifKodu+"-xl7.jpg";
    					
    					
    				}
    				
    				threadNesnesiAlinan.sendEmptyMessage(1);
    				
    				
    				
    				
    			}
    			else
    			{
    				threadNesnesiAlinan.sendEmptyMessage(-1);

    			}
    			
			} catch (IOException e) {

				threadNesnesiAlinan.sendEmptyMessage(-1);
				Log.e("hataa", e.getMessage());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.e("hataa", e.getMessage());
				threadNesnesiAlinan.sendEmptyMessage(-1);
				e.printStackTrace();
			}
    		
    		
    		
    	}
    	
    }
    
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gruplar_desenler);
		
		
		
		gruplarJsonAl gja= new gruplarJsonAl(threadNesnesi,getApplicationContext());
		gja.start();
		
		
		
		
		
	}
	
	
}
