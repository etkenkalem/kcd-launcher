package com.kioskmaker;

import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

/*
 * http://www.andreas-schrade.de/2015/02/16/android-tutorial-how-to-create-a-kiosk-mode-in-android/
 * 
 * */



public class MainActivity extends Activity {
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		View decorViewx = getWindow().getDecorView();
		decorViewx.setOnSystemUiVisibilityChangeListener
		        (new View.OnSystemUiVisibilityChangeListener() {
		    @Override
		    public void onSystemUiVisibilityChange(int visibility) {
		        // Note that system bars will only be "visible" if none of the
		        // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
		        if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
		            // TODO: The system bars are visible. Make any desired
		            // adjustments to your UI, such as showing the action bar or
		            // other navigational controls.
		        	
		        	Log.e("ekran", "ekran de�i�ti kapal�");
		        	
		        	
		        } else {
		            // TODO: The system bars are NOT visible. Make any desired
		            // adjustments to your UI, such as hiding the action bar or
		            // other navigational controls.
		        	Log.e("ekran", "ekran de�i�ti a��k");
		        }
		    }
		});
		
		
		
		
		
		
		
		
		
		
		if (Build.VERSION.SDK_INT < 16) {
			   getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
			 } else {
				 try
				 {
			     View decorView = getWindow().getDecorView();
			      int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
			      decorView.setSystemUiVisibility(uiOptions);
			      ActionBar actionBar = getActionBar();
			      actionBar.hide();
				 }catch(Exception ex)
				 {
					 
				 }
			 }
		
		if(Build.VERSION.SDK_INT>=14)
		{
			
			try
			{
			View decorView = getWindow().getDecorView();
			// Hide both the navigation bar and the status bar.
			// SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
			// a general rule, you should design your app to hide the status bar whenever you
			// hide the navigation bar.
			int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
			              | View.SYSTEM_UI_FLAG_FULLSCREEN;
			decorView.setSystemUiVisibility(uiOptions);
			ActionBar actionBar = getActionBar();
		      actionBar.hide();
			}
			catch(Exception ex)
			{
				
			}
		}
	
		gizle();
		
		setContentView(R.layout.activity_main);
		
	
		
		
		SharedPreferences sp = getSharedPreferences("kcdData",Context.MODE_PRIVATE);
		
		if(!sp.getBoolean("kayitlimi", false))
		{
			loginCagir();
		}
		
		
		
		
		
		
		
		
		
	}
	
	public void loginCagir()
	{
		Intent niyet= new Intent(getApplicationContext(),LoginActivity.class);
		startActivityForResult(niyet, 1);
		
	}
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		
		
	}
	
	
	public void gizle()
	{
		if(Build.VERSION.SDK_INT>=16)
		{
			View decorView = getWindow().getDecorView();
			decorView.setSystemUiVisibility(
		            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
		            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
		            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
		            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
		            | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
		            | View.SYSTEM_UI_FLAG_IMMERSIVE);
			
//			decorView.setOnSystemUiVisibilityChangeListener
//	        (new View.OnSystemUiVisibilityChangeListener() {
//			    @Override
//			    public void onSystemUiVisibilityChange(int visibility) {
//			    	View decorView = getWindow().getDecorView();
//			        // Note that system bars will only be "visible" if none of the
//			        // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
//			        if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
//			            // TODO: The system bars are visible. Make any desired
//			            // adjustments to your UI, such as showing the action bar or
//			            // other navigational controls.
//			        	decorView.setSystemUiVisibility(
//					            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//					            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//					            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//					            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
//					            | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
//					            | View.SYSTEM_UI_FLAG_IMMERSIVE);
//			        } else {
//			            // TODO: The system bars are NOT visible. Make any desired
//			            // adjustments to your UI, such as hiding the action bar or
//			            // other navigational controls.
//			        	decorView.setSystemUiVisibility(
//					            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//					            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//					            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//					            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
//					            | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
//					            | View.SYSTEM_UI_FLAG_IMMERSIVE);
//			        }
//			    }
//			});
			
			
			
		
		}
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		// TODO Auto-generated method stub
		Log.e("test@@@@@", "@@@@@fokushokus");
		
		super.onWindowFocusChanged(hasFocus);
		
		super.onWindowFocusChanged(hasFocus);

        Log.d("Focus debug", "Focus changed !");

		    if(!hasFocus) {
		        Log.d("Focus debug", "Lost focus !");
		
		        Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
		        sendBroadcast(closeDialog);
		    }
		
		
		
		Log.e("test", "fokushokus");
		gizle();
	}
	
	@Override
	  public void onBackPressed() {
	      // nothing to do here
	      // � really      
	  }
	  
	
	
	
	public void uygulamalariOldur()
	{
		
		
		Button runningApp = (Button) findViewById(R.id.buton_ayarlar);
		runningApp.setOnClickListener(new View.OnClickListener()
		{
		    @Override
		    public void onClick(View v)
		    {
		        String nameOfProcess = "com.example.filepath";
		        ActivityManager  manager = (ActivityManager)MainActivity.this.getSystemService(Context.ACTIVITY_SERVICE);
		        List<ActivityManager.RunningAppProcessInfo> listOfProcesses = manager.getRunningAppProcesses();
		        for (ActivityManager.RunningAppProcessInfo process : listOfProcesses)
		        {
		            if (process.processName.contains(nameOfProcess))
		            {
		                Log.e("Proccess" , process.processName + " : " + process.pid);
		                android.os.Process.killProcess(process.pid);
		                android.os.Process.sendSignal(process.pid, android.os.Process.SIGNAL_KILL);
		                manager.killBackgroundProcesses(process.processName);
		                break;
		            }
		        }
		    }
		});
		
	}
	
	public void gruplar_desenler(View v){
		
		Intent intent = new Intent(getBaseContext(),GruplarActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}
	public void ayarlar(View v)
	{
//		 ActivityManager am = (ActivityManager) getSystemService(Activity.ACTIVITY_SERVICE);
//	    am.killBackgroundProcesses(packageName);
		
		//uygulamalariOldur();
		
		Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
		//Intent intent = new Intent(Settings.ACTION_HOME_SETTINGS);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		
	}
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu; this adds items to the action bar if it is present.
	    getMenuInflater().inflate(R.menu.settings, menu);
	    return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle action bar item clicks here. The action bar will
	    // automatically handle clicks on the Home/Up button, so long
	    // as you specify a parent activity in AndroidManifest.xml.
	    int id = item.getItemId();
	    if (id == R.id.action_settings) {
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
	
}
