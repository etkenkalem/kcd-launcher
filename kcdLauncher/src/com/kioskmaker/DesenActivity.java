package com.kioskmaker;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.kioskmaker.DesenlerActivity.desenlerJsonAl;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import custom_elements.CustomAdapter;
import custom_elements.CustomGrid;
import custom_elements.ListModel;
import horizontal_listview.ListViewHorizontal;
import kcdnetlibrary.DosyaIndirici;
import kcdnetlibrary.GenelSabitler;
import kcdnetlibrary.HttpGetYardimci;
import kcdnetlibrary.NetParametresi;

public class DesenActivity extends Activity {

	public static long muhatabGrupId=0;
	public static long[] muhatabDesenIdleri;
	public static String[] muhatabDesenResimleri;
	public static String[] muhatabDesenIsimleri;
	public static long muhatabDesenId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_desen);

		Bundle gelenDeger= getIntent().getExtras();
		long itemId= gelenDeger.getLong("itemId");
		muhatabDesenId=itemId;
				muhatabDesenIdleri=	gelenDeger.getLongArray("idler");
				muhatabDesenIsimleri=gelenDeger.getStringArray("isimler");
				muhatabDesenResimleri=gelenDeger.getStringArray("resimler");
		
		// yatay liste i�in buray� desenlerle dolduracaz
		ListViewHorizontal listview = (ListViewHorizontal) findViewById(R.id.listViewYatay);
		listview.setAdapter(mAdapter);
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
				
				ImageView resim= (ImageView) findViewById(R.id.imgSol);
				Bitmap bitmap;
				try {
					bitmap = BitmapFactory.decodeStream(getApplicationContext().openFileInput(muhatabDesenResimleri[position]
							), null, null);
					resim.setImageBitmap(bitmap);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					Log.e("hata_tekresimde", e.getMessage());
					e.printStackTrace();
				}
            
        	  
        	  
			
				
				
				urunlerJsonAl gja= new urunlerJsonAl(threadNesnesi,getApplicationContext(),String.valueOf(muhatabDesenIdleri[position]));
				gja.start();
				
			}
			
			
		});
		
		// yatay liste i�in

		
		
		
		
//		// kucuk yatay liste i�in ba�ta desen ve desenin di�er resimleri ile doldurulacak
//		//e�er bir �r�ne t�klan�rsa �r�n�n resmi ve di�er resimleri ile dolacak
//		//e�er bir desene t�klan�rsa desen ve desenin di�er reimleri ile dolacak
//		ListViewHorizontal listviewKucuk = (ListViewHorizontal) findViewById(R.id.listViewYatayKucuk);
//		listviewKucuk.setAdapter(mAdapterKucuk);
//		// kucuk yatay liste i�in

		
		
		
		
		
		// grid i�in buray� �r�nlerle dolduracaz
		
		urunlerJsonAl gja= new urunlerJsonAl(threadNesnesi,getApplicationContext(),String.valueOf(itemId));
		gja.start();
		// grid i�in

	}

	// yatay liste i�in a�a��s�

	
	
	
	
	private BaseAdapter mAdapter = new BaseAdapter() {

		// private OnClickListener mOnButtonClicked = new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// AlertDialog.Builder builder = new
		// AlertDialog.Builder(DesenActivity.this);
		// builder.setMessage("hello from " + v);
		// builder.setPositiveButton("Cool", null);
		// builder.show();
		//
		// }
		// };

		
		
		
		
		@Override
		public int getCount() {
			return muhatabDesenIsimleri.length;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View retval = LayoutInflater.from(parent.getContext()).inflate(R.layout.horiztontal_list_item, null);
			ImageView resim = (ImageView) retval.findViewById(R.id.imgHorizontalView);
			TextView title = (TextView) retval.findViewById(R.id.title);
			// Button button = (Button) retval.findViewById(R.id.clickbutton);
			// button.setOnClickListener(mOnButtonClicked);
			title.setText(muhatabDesenIsimleri[position]);
//			resim.setImageDrawable(loadDrawableFromAssets(getApplicationContext(),
//					"kcd_resimler/desenler/d" + String.valueOf((position + 1)) + ".jpg"));

			
			
			
			if(!muhatabDesenResimleri[position].contains("resimyok"))
	              try {
	            	  
	            	  FileInputStream fis;
	      			
	  				fis = getApplicationContext().openFileInput(muhatabDesenResimleri[position]
	  							);
	  			 
	                Bitmap bitmapx;
	                BitmapFactory.Options options = new BitmapFactory.Options();
	                options.inJustDecodeBounds = true;
	                bitmapx = BitmapFactory.decodeStream(fis, null, options);
	                
	                int imageHeight = options.outHeight;
	                int imageWidth = options.outWidth;
	                Log.e("yukl", String.valueOf(imageHeight));
	                Log.e("width", String.valueOf(imageWidth));
	                
	                
	                // recreate the stream
	                // make some calculation to define inSampleSize
	                
	                
	                DisplayMetrics metrics = new DisplayMetrics();
	                
	                ((Activity)DesenActivity.this).getWindowManager().getDefaultDisplay().getMetrics(metrics);
	                int screenWidth = metrics.widthPixels;
	                int screenHeight =metrics.heightPixels;
	                
	                
	                
	                options.inSampleSize = calculateInSampleSize(options,screenWidth/4,screenHeight/3);
	                options.inJustDecodeBounds = false;
	                options.inPreferredConfig = Config.RGB_565;
	                
	                Bitmap bitmap = BitmapFactory.decodeStream(getApplicationContext().openFileInput(muhatabDesenResimleri[position]
								), null, options);
	                
	            	  
	            	  
					resim.setImageBitmap(bitmap);
					
					 imageHeight = options.outHeight;
		                imageWidth = options.outWidth;
		                Log.e("yuklyy", String.valueOf(imageHeight));
		                Log.e("widthyy", String.valueOf(imageWidth));
		                
		                
				} catch (Exception e) {
					resim.setImageDrawable(loadDrawableFromAssets(getApplicationContext(),
							"kcd_resimler/desenler/d1.jpg"));
		            	  
					// TODO Auto-generated catch block
					Log.e("hatabune", e.getMessage());
					//e.printStackTrace();
				}
	              else
	              {
	            	  
//	            	  resim.setImageBitmap(loadBitmapFromAssets(getApplicationContext(), "kcd_resimler/gruplar/g1.jpg"));
	            	  
	      			resim.setImageDrawable(loadDrawableFromAssets(getApplicationContext(),
						"kcd_resimler/desenler/d1.jpg"));
	            	  
	              }
	              
			
						
			
			
			
			
			
			
			return retval;
		}

	};
	// yatay liste i�in

	
	
	
	// kucuk yatay liste i�in a�a��s�

	private static String[] dataObjectsKucuk = new String[] { "EL�T", "GLORIA PLATIN", "BRIGT ALTIN" };

	private BaseAdapter mAdapterKucuk = new BaseAdapter() {

		// private OnClickListener mOnButtonClicked = new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// AlertDialog.Builder builder = new
		// AlertDialog.Builder(DesenActivity.this);
		// builder.setMessage("hello from " + v);
		// builder.setPositiveButton("Cool", null);
		// builder.show();
		//
		// }
		// };

		@Override
		public int getCount() {
			return dataObjectsKucuk.length;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View retval = LayoutInflater.from(parent.getContext()).inflate(R.layout.horiztontal_list_item_small_images, null);
			ImageView resim = (ImageView) retval.findViewById(R.id.imgHorizontalViewSmall);
			TextView title = (TextView) retval.findViewById(R.id.titleSmall);
			// Button button = (Button) retval.findViewById(R.id.clickbutton);
			// button.setOnClickListener(mOnButtonClicked);
			title.setText(dataObjectsKucuk[position]);
			resim.setImageDrawable(loadDrawableFromAssets(getApplicationContext(),
					"kcd_resimler/desenler/d" + String.valueOf((position + 1)) + ".jpg"));

			
			
			
			return retval;
		}

	};
	// kucuk yatay liste i�in

	private Drawable loadDrawableFromAssets(Context context, String path) {
		InputStream stream = null;
		try {
			stream = context.getAssets().open(path);
			return Drawable.createFromStream(stream, null);
		} catch (Exception ignored) {

			Log.e("hataaa", ignored.getMessage());

		} finally {

			try {
				if (stream != null) {
					stream.close();
				}
			} catch (Exception ignored) {
			}
		}
		return null;
	}

	
	 
    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
    // Raw height and width of image
    final int height = options.outHeight;
    final int width = options.outWidth;
    int inSampleSize = 1;
 
    if (height > reqHeight || width > reqWidth) {
 
        // Calculate ratios of height and width to requested height and width
        final int heightRatio = Math.round((float) height / (float) reqHeight);
        final int widthRatio = Math.round((float) width / (float) reqWidth);
 
        // Choose the smallest ratio as inSampleSize value, this will guarantee
        // a final image with both dimensions larger than or equal to the
        // requested height and width.
        inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
    }
 
    return inSampleSize;
}
    
    
	
	
	
	
	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!gridview!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

GridView grid;
    
	String[] isim = {
            "2013 COLLECTIONS",
            "KONSOL GRUBU",
            "�AY BAH�ES� GRUBU",
            "OBJE GRUBU",
            "PORSELEN SOFRA GRUBU",
            "SOFRA BARDAK & SET GRUBU" 
    };
    
    long[] itemId = {
            R.drawable.ayarlar,
            R.drawable.kampanyalar,
            R.drawable.hakkimizda,
            R.drawable.kcdlogo,
            R.drawable.sikayet,
            R.drawable.urun2
    };
    
    String[] assestsImages={
    		"kcd_resimler/gruplar/g1.jpg",
    		"kcd_resimler/gruplar/g2.jpg",
    		"kcd_resimler/gruplar/g3.jpg",
    		"kcd_resimler/gruplar/g4.jpg",
    		"kcd_resimler/gruplar/g5.jpg",
    		"kcd_resimler/gruplar/g6.jpg",    		
    };
 
	
    public void gridiDoldur()
    {
    	CustomGrid adapter = new CustomGrid(DesenActivity.this,
    			DesenActivity.this,
    			isim, itemId,assestsImages);
        grid=(GridView)findViewById(R.id.grid_digerleri);
        
                grid.setAdapter(adapter);
                grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
 
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        Toast.makeText(DesenActivity.this, "T�klanan " +isim[position], Toast.LENGTH_SHORT).show();
                        
                        ImageView resim= (ImageView) findViewById(R.id.imgSol);
                       
        				Bitmap bitmap;
        				try {
        					bitmap = BitmapFactory.decodeStream(getApplicationContext().openFileInput(assestsImages[position]
        							), null, null);
        					resim.setImageBitmap(bitmap);
        				} catch (FileNotFoundException e) {
        					// TODO Auto-generated catch block
        					Log.e("hata_tekresimde", e.getMessage());
        					//e.printStackTrace();
        				}
                    
                        
                        
 
                    }
                });
    	
    }
    
    public Handler threadNesnesi= new Handler(){
    	
    	public void handleMessage(android.os.Message msg) {
    	
    		switch (msg.what) {
			case 1:
			{
				gridiDoldur();
				
				break;
			}
			case -1:
			{
				
				break;
			}

			default:
				break;
			}
    		
    		
    	};
    	
    };
   
    
    
    public class urunlerJsonAl extends Thread
    {
    	Handler threadNesnesiAlinan;
    	Context ctx;
    	String desenIdGelen;
    	public urunlerJsonAl(Handler threadNesnesiGelen,Context ctx,String desenId) {
		
    		this.threadNesnesiAlinan=threadNesnesiGelen;
    		this.ctx=ctx;
    		this.desenIdGelen=desenId;
		}
    	
    	@Override
    	public void run() {
    	
    		super.run();
    		
    		HttpGetYardimci hy= new HttpGetYardimci();
    		String GET_URL=GenelSabitler.hostAdresi+"/api/kioskAndroid/urunler";
    		NetParametresi[] parametreler= new NetParametresi[3];    		
    		
    		NetParametresi npSifre= new NetParametresi();
    		npSifre.parametreIsmi="sifre";
    		NetParametresi npMac= new NetParametresi();
    		npMac.parametreIsmi="mac";    		
    		NetParametresi npDesenId= new NetParametresi();
    		npDesenId.parametreIsmi="desenId";
    		
    		
    		SharedPreferences sp= getSharedPreferences("kcdData", Context.MODE_PRIVATE);
    		
    		
    		
    		npSifre.parameterDegeri= sp.getString("sifre", "");
    		npMac.parameterDegeri= sp.getString("mac", "");
    		npDesenId.parameterDegeri=desenIdGelen;
    		
    		Log.e("sifre", npSifre.parameterDegeri);
    		Log.e("sifre", npMac.parameterDegeri);
    		
    		parametreler[0]=npMac;
    		parametreler[1]=npSifre;
    		parametreler[2]=npDesenId;
    		
    		
    		
    		try {
				
    			String sonuc= hy.getSorgusuYap(GET_URL, parametreler);
    			
    			sonuc= sonuc.substring(1,sonuc.length()-1).replace("\\","");
    			
    			Log.e("data", sonuc);
    			
    			JSONObject jObj= new JSONObject(sonuc);
    			
    			String durum= jObj.getString("durum");
    			if(!durum.contains("basarisiz"))
    			{
    				
    				JSONArray dizi=jObj.getJSONArray("veri");
    				
    				Log.e("dizi", dizi.toString());
    				
    				int diziBoyutu= dizi.length();
    				
    				isim= new String[diziBoyutu];
    				itemId= new long[diziBoyutu];
    				assestsImages= new String[diziBoyutu];
    				
    				for(int i=0; i<diziBoyutu;i++)
    				{
    					JSONObject item= dizi.getJSONObject(i);
    					Log.e("Jarrayobje", item.toString());
    					
    					isim[i]=item.getString("UrunAdi_utf8");
    					itemId[i]=item.getLong("UrunID");
    					
    					String UrunKodu=item.getString("UrunKodu");
    					
    					try
    					{
    					DosyaIndirici.downloadFileForAndroid("http://www.kcd.com.tr/urunfoto/"+UrunKodu+"-xl7.jpg", "gruplar", getApplicationContext());
    					assestsImages[i]="gruplar_"+UrunKodu+"-xl7.jpg";
    					}
    					catch(Exception ex)
    					{
    						Log.e("resim indirilirken hata", "hata@@@@@@@@"+ex.getMessage());
    						assestsImages[i]="resimyok";
    					}
    					
    					
    					//TODO de�i�tirilecek
    					//assestsImages[i]="kcd_resimler/gruplar/g1.jpg";
    					//assestsImages[i]="gruplar/"+UrunSinifKodu+"-xl7.jpg";
    					
    					
    				}
    				
    				threadNesnesiAlinan.sendEmptyMessage(1);
    				
    				
    				
    				
    			}
    			else
    			{
    				threadNesnesiAlinan.sendEmptyMessage(-1);

    			}
    			
			} catch (IOException e) {

				threadNesnesiAlinan.sendEmptyMessage(-1);
				Log.e("hataa", e.getMessage());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.e("hataa", e.getMessage());
				threadNesnesiAlinan.sendEmptyMessage(-1);
				e.printStackTrace();
			}
    		
    		
    		
    	}
    	
    }
   
	
	
	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!gridview!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

}
