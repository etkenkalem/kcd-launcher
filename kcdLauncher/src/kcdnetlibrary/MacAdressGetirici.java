package kcdnetlibrary;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.util.Log;

public class MacAdressGetirici {

	
	public static String macGetir(Context context) throws Exception
	{
//		<uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
		
		    WifiManager wimanager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		    String macAddress = wimanager.getConnectionInfo().getMacAddress();
		    if (macAddress == null) {
		    	
		    	Log.e("mac", "mac adresi null geldi");
		    	
		       throw new Exception("MAC Adresine Ulaşılamıyor");
		    }
		    return macAddress;
		
	}
	
	
}
