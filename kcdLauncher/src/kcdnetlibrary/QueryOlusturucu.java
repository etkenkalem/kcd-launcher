package kcdnetlibrary;

public class QueryOlusturucu {

	
	
	public static String olusturQuery(NetParametresi... parametreler)
	{
		String sorgu="";
		
		if(parametreler!=null)
		{
			sorgu+="?";
			
			int parametreSayi=parametreler.length;
					
			
			for(int i=0;i<parametreSayi;i++)
			{
				
				sorgu+=parametreler[i].parametreIsmi;
				sorgu+="=";
				sorgu+=parametreler[i].parameterDegeri;
				
				if((i+1)!=parametreSayi)
				{
					sorgu+="&";
					
				}
				
			}
			
		}
		
		
		return sorgu;
		
	}
}
