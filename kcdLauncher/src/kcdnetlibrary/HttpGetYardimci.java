package kcdnetlibrary;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import android.util.Log;

public class HttpGetYardimci {

	
	
	private static final String USER_AGENT = "Mozilla/5.0";
	 
//    private static final String GET_URL = "http://localhost:9090/SpringMVCExample";
 
    private static final String POST_URL = "http://localhost:9090/SpringMVCExample/home";
 
    private static final String POST_PARAMS = "userName=Pankaj";
	
	
	public String getSorgusuYap(String GET_URL, NetParametresi... parametreler) throws IOException
	{
		URL obj = new URL(GET_URL + QueryOlusturucu.olusturQuery(parametreler));
		Log.e("1","11");
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		Log.e("1","2");
        con.setRequestMethod("GET");
//        con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
        con.setRequestProperty("User-Agent", USER_AGENT);
        Log.e("1","3");
        int responseCode=0;
        try
        {
        responseCode= con.getResponseCode();
        }
        catch(Exception ex)
        {
        	Log.e("1",GET_URL + QueryOlusturucu.olusturQuery(parametreler));
        	Log.e("1",ex.getMessage());
        }
        Log.e("1","4");
        Log.e("get responso code gelen", "GET Response Code :: " + responseCode);
        Log.e("1","5");
        StringBuffer response = new StringBuffer();
        if (responseCode == HttpURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;
            
 
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
 
            // print result
            Log.e("get sonuc", response.toString());
        } else {
        	Log.e("get", "GET request not worked");
            throw new IOException("Ba�lant� Kurulamad�...");
        }
		
		return response.toString();
		
	}
	
	public String postSorgusuYap(NetParametresi... parametreler) throws IOException
	{
		
		
		URL obj = new URL(POST_URL);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
 
        // For POST only - START
        con.setDoOutput(true);
        OutputStream os = con.getOutputStream();
        os.write(POST_PARAMS.getBytes());
        os.flush();
        os.close();
        // For POST only - END
 
        int responseCode = con.getResponseCode();
        System.out.println("POST Response Code :: " + responseCode);
 
        if (responseCode == HttpURLConnection.HTTP_OK) { //success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
 
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
 
            // print result
            System.out.println(response.toString());
        } else {
            System.out.println("POST request not worked");
        }
		
		
		
		return "";
	}
	
}
